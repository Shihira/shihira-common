#!/usr/bin/python

import os, sys, getopt

def get_binary_string(filename):
        file = open(filename, "rb")
        file_size = os.path.getsize(filename)
        file_content = file.read(file_size)
        converted = ""

        for i, char in enumerate(file_content):
                if i % 16 == 0:
                        converted += "\n        "
                converted += "0x{0:02X}".format(ord(char))
                if i < file_size - 1:
                        converted += ", "

        file.close()
        return (converted, file_size)

def view_help():
        print \
r'''
Binary to Header by Shihira Fung

USAGE:
       python binary_to_header.py -f input -o output -v variable -h

OPTIONS:
       -f input:       set the input file/path.(required)
       -o output:      set the output file/path.(optional)
       -v variable:    set the variable.(optional)
       -h:             help

NOTICE:
       If output or variable not set, output will be input.h,
       and variable will be the same as the input file name.
       No matter what the variable was, the data will be named
       variable_data and the size will be named variable_size.
       A bit complex... You will understand when seeing the header
'''
        sys.exit(0)

def get_argument(argv):
        f = ""
        o = ""
        v = ""
        s = False
        h = False

        arg_list = getopt.getopt(argv[1:], "f:o:v:sh", ["help"])[0]
        for arg, content in arg_list:
                if arg == "-f":
                        f = content
                elif arg == "-o":
                        o = content
                elif arg == "-v":
                        v = content
                elif arg == "-s":
                        s = True
                elif arg in ("-h", "--help"):
                        h = True

        if h:
                view_help()
        if f == "":
                print "Er... You didn't seem to input the filename to convert."
                print "Get help using --help of -h or input filename here:"
                f = raw_input("(input an empty line to abort)")
                if f == "":
                        exit(0)
        if o == "":
                o = f + ".h"
        if v == "":
                # non-alnum character mustn't occur in varname in C
                # the first char mustn't be digit
                p_v = ""
                v = os.path.split(f)[1]
                for char in v:
                        if char.isalnum():
                                p_v += char
                        else:
                                p_v += "_"
                        v = p_v
                if v[0].isdigit():
                        v = "_" + v

        return (f, o, v, s)

def main(argv):
        filename_in, \
        filename_out, \
        var_name, \
        silent_mode = get_argument(argv)

        bin_str, file_size = get_binary_string(filename_in)

        file_str = r'''
/********************************************\
 * The file is created by Binary to Header. *
 * Binary_to_header's author is Shihira.    *
 * I Have opened it's source code.          *
 * E-mail : fengzhiping@hotmail.com         *
\********************************************/


'''
        file_str += "size_t " + var_name + "_size = " + str(file_size) + ";\n"
        file_str += "unsigned char " + var_name + "[" + str(file_size) + "] = {"
        file_str += bin_str
        file_str += " };"

        file = open(filename_out, "w")
        file.write(file_str)
        file.close()

main(sys.argv)
