#include "../sinc/netfile.h"

// g++ netfile_test.cc -o netfile_test.exe -I../../curl/include -L../../curl -lcurl -lws2_32 -lwsock32 -lwldap32

int main(int argc, char* argv[])
{
        netfile file;
        file.local = "rfc0001.txt";
        file.remote = "http://www.ietf.org/rfc/rfc0001.txt";
        file.log = "log.txt";
        file.timeout = 5000;
        file.download();
}

