#include <iostream>
#include "../sinc/xml_easya.h"

// g++ xml_easya_test.cc -o xml_easya_test.exe -I../../tinyxml/include -L../../tinyxml -ltinyxml

int main(int argc, char* argv[])
{
        TiXmlDocument doc;
        doc.LoadFile("origin.xml");
        xml_easya xml(doc);

        std::cout << xml["data1"].text() << std::endl;
        xml["data2"] += "789";
        // NOTICE: xml["data2"] = xml["data2"].text() + "789"   RIGHT
        //         xml["data2"] = xml["data2"] + "789"          WRONG
        xml["data3"] = "123456789";
        xml["data4"] += xml_easya("data4_1", "123456789");
        // NOTICE: xml["data4"]["data4_1"] = "123456789";       RIGHT
        std::cout << xml["data5"]("num") << std::endl;

        doc.SaveFile("modified.xml");
}
