#include <iostream>
#include <vector>

// g++ xml_bind_test.cc -o xml_bind_test.exe -I../../tinyxml/include -L../../tinyxml -ltinyxml

#include "../sinc/xml_bind.h"

int main(int argc, char* argv[])
{
        TiXmlDocument doc;
        doc.LoadFile("bind_info.xml");
        xml_easya xml(doc);
        xml_bind binder(xml);

        std::string data1;
        int data2;
        std::vector<std::string> data3;

        binder.bind("data1", data1);
        binder.bind("data2", data2);
        binder.bind("data3", data3);

        binder.update();

        std::cout << data1 << std::endl;
        data1 += " without period";
        std::cout << data2 << std::endl;
        data2 -= 5536;
        for(int i = 0; i < data3.size(); i++)
                std::cout << data3[i] << std::endl;
        data3[0] = "321";

        binder.flush();
        doc.SaveFile("bind_info_mod.xml");
}
