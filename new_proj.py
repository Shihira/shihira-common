#!/usr/bin/env python

import os
import sys

dirname = None
common_dir = os.path.dirname(os.path.abspath(__file__))

for arg in sys.argv[1:]:
        if arg: dirname = arg; break
if not dirname: dirname = raw_input \
        ("Project directory (pwd: %s)" % os.getcwd())

if not os.path.exists(dirname):
        os.mkdir(dirname)
        print "Created directory " + dirname
if not os.path.exists(dirname + "/build"):
        os.mkdir(dirname + "/build")
        print "Created directory " + dirname + "/build"
if not os.path.exists(dirname + "/include"):
        os.mkdir(dirname + "/include")
        print "Created directory " + dirname + "/include"
if not os.path.exists(dirname + "/src"):
        os.mkdir(dirname + "/src")
        print "Created directory " + dirname + "/src"


if not os.path.exists(dirname + "/build/Shihira.cmake"):
        shihira_cmake = open(dirname + "/build/Shihira.cmake", "w")
        shihira_cmake.write('INCLUDE({0}/cmake/Shihira.cmake)\n'
                'SET(SHIHIRA_COMMON_DIR {0})'.format(common_dir))
        shihira_cmake.close()

if not os.path.exists(dirname + "/build/CMakeLists.txt"):
        cmakelists = open(dirname + "/build/CMakeLists.txt", "w")
        cmakelists.write('CMAKE_MINIMUM_REQUIRED(VERSION 2.6)\n'
                'INCLUDE(Shihira.cmake)')
        cmakelists.close()
