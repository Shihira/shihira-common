#ifndef  XML_BIND_INC
#define  XML_BIND_INC

#include <tinyxml.h>

#include <map>
#include <vector>
#include <string>

#include "string_ext.h"
#include "xml_easya.h"

class xml_bind_base {
public:
        typedef std::vector<std::string> astr_t;

protected:
        typedef std::map<std::string, astr_t*> astr_map_t;
        typedef std::map<std::string, std::string*> str_map_t;

        astr_map_t astr_map;
        str_map_t str_map;

        xml_easya* m_xe;

        xml_bind_base() { m_xe = 0; }
public:
        xml_bind_base(const xml_easya& xe) { m_xe = new xml_easya(xe); }
        ~xml_bind_base() { if(m_xe) delete m_xe; }

        void bind(const std::string& name, std::string& to_bind) { str_map[name] = &to_bind; }
        void bind(const std::string& name, astr_t& to_bind) { astr_map[name] = &to_bind; }

        virtual void flush() {
                // flush strings
                for(str_map_t::const_iterator m_i = str_map.begin();
                    m_i != str_map.end(); m_i++) {
                        (*m_xe)[m_i->first] = *m_i->second;
                }

                // update vectors
                for(astr_map_t::iterator m_i = astr_map.begin();
                    m_i != astr_map.end(); m_i++) {
                        const std::string& name = m_i->first;
                        astr_t& astr = *(m_i->second);

                        int num = m_xe->how_many(name);
                        for(int a_i = 0; a_i < num; a_i++)
                                (*m_xe)[name][a_i] = astr[a_i];
                }
        }

        virtual void update() {
                // update strings
                for(str_map_t::const_iterator m_i = str_map.begin();
                    m_i != str_map.end(); m_i++) {
                        m_i->second->assign((*m_xe)[m_i->first].text());
                }

                // update vectors
                for(astr_map_t::iterator m_i = astr_map.begin();
                    m_i != astr_map.end(); m_i++) {
                        const std::string& name = m_i->first;
                        astr_t& astr = *(m_i->second);

                        int num = m_xe->how_many(name);
                        astr.clear();
                        for(int a_i = 0; a_i < num; a_i++)
                                astr.push_back((*m_xe)[name][a_i].text());
                }
        }
};

class xml_bind : public xml_bind_base {
public:
protected:
        typedef std::map<int*, std::string> int_map_t;
        int_map_t int_map;

        xml_bind() : xml_bind_base() { }
public:
        xml_bind(const xml_easya& xe) : xml_bind_base(xe) { }

        void bind(const std::string& name, std::string& to_bind) {
                xml_bind_base::bind(name, to_bind); }
        void bind(const std::string& name, astr_t& to_bind) {
                xml_bind_base::bind(name, to_bind); }
        void bind(const std::string& name, int& to_bind) {
                std::string& str_to_bind = int_map[&to_bind];
                bind(name, str_to_bind); }

        virtual void update() {
                xml_bind_base::update();
                for(int_map_t::iterator i = int_map.begin();
                    i != int_map.end(); i++) {
                        *(i->first) = string_to_int(i->second);
                }
        }

        virtual void flush() {
                for(int_map_t::iterator i = int_map.begin();
                    i != int_map.end(); i++) {
                        std::string num;
                        string_format(num, "%d", *(i->first));
                        i->second = num;
                }
                xml_bind_base::flush();
        }

};

class xml_bind_file : private xml_bind {
protected:
        TiXmlDocument m_doc;

public:
        xml_bind_file() : xml_bind() { }
        void load_file(const std::string& filename) {
                m_doc.LoadFile(filename);
                m_xe = new xml_easya(m_doc);
                update();
        }

        void save_file(const std::string& filename = "") {
                if(m_xe) {
                        flush();
                        if(filename.empty())
                                m_doc.SaveFile();
                        else    m_doc.SaveFile(filename);
                }
        }

protected:
        void connect_string(const std::string& name, std::string& var)
                { bind(name, var); } 
        void connect_integer(const std::string& name, int& var)
                { bind(name, var); } 
};

#endif   /* ----- #ifndef XML_BIND_INC  ----- */

