#ifndef  XML_EASYA_INC
#define  XML_EASYA_INC

#include <string>
#include <tinyxml.h>

class xml_easya {
        friend class xml_bind;

protected:
        TiXmlElement* m_this;

        xml_easya(TiXmlElement* t) { m_this = t;}
public:
        // constructor
        xml_easya(const xml_easya& xe) { m_this = xe.m_this; }
        xml_easya(TiXmlDocument& doc) { m_this = doc.RootElement(); }
        xml_easya(const std::string& name, const std::string& str) {
                m_this = new TiXmlElement(name);
                operator=(str);
        }

        // operator
        xml_easya operator[](const std::string& name) {
                TiXmlElement* element = m_this->FirstChildElement(name);
                // ensure that node exsits
                if(!element) {
                        element = new TiXmlElement(name);
                        m_this->LinkEndChild(element);
                }
                return element;
        }

        xml_easya operator[] (int index) {
                TiXmlElement* element = m_this;
                for(int i = 0; i < index && element; i++) {
                        element = (TiXmlElement*)element->NextSibling(m_this->ValueStr());
                        // ensure that node exsits
                        if(!element) {
                                element = new TiXmlElement(m_this->ValueStr());
                                m_this->Parent()->LinkEndChild(element);
                        }
                }
                return element;
        }

        // TiXmlElement* operator->() const { return m_this; } 
        void operator=(const std::string& text) { m_this->Clear(); operator+=(text); }
        void operator=(const xml_easya& node) { m_this->Clear(); operator+=(node); }
        void operator+=(const std::string& text) { TiXmlText* node = new TiXmlText(text); m_this->LinkEndChild(node); }
        void operator+=(const xml_easya& node) { m_this->LinkEndChild(node.m_this); }
        const std::string& operator()(const std::string& attr) const { return *(m_this->Attribute(attr)); }

        // functions
        int how_many(const std::string& name) const {
                const TiXmlElement* element = m_this->FirstChildElement(name);
                int counter = 0;
                while(element) {
                        element = (TiXmlElement*)element->NextSibling(name);
                        counter++; // prev element is available
                }
                return counter;
        }

        std::string text() const {
                const char* ret = m_this->GetText();
                if(ret) return ret;
                else return "";
        }
};


#endif   /* ----- #ifndef XML_EASYA_INC  ----- */

