#ifndef  STRING_EXT_INC
#define  STRING_EXT_INC

#include <string>
#include <cstdarg>
#include <cstdio>

inline int string_format(std::string& stdstr, const std::string& fmt, ...) {
        int size = 64;
twice:  char* str = new char[size];
        va_list ap; va_start(ap, fmt);

        int n = std::vsnprintf(str, size, fmt.c_str(), ap);
        if(n < size) {
                stdstr = str;
                delete[] str;
                return n;
        } else {
                size = n + 1;
                goto twice;
        }
}


inline int string_to_int(const std::string& string) {
        int num = 0;
        bool is_prev_num = false;
        short int plus_minus = 1;
        const char* data = string.data();
        for(int i = 0; i < string.size(); i++) {
                if((data[i] > '9' || data[i] < '0') && is_prev_num) break;
                if(data[i] >= '0' && data[i] <= '9') {
                        num = num * 10 + data[i] - '0';
                        if(data[i - 1] == '-')
                                plus_minus = -1;
                }
        }
        return plus_minus * num;
}

#endif   // ----- #ifndef STRING_EXT_INC  -----
