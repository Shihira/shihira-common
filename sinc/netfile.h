#ifndef  netfile_INC
#define  netfile_INC

#define CURL_STATICLIB

#include <curl/curl.h>
#include <stdio.h>
#include <time.h>
#include <vector>
#include <string>

#define verify_perform(x) if(x) { release();return x; }

class netfile {
private:
protected:
        CURL* m_curl;

        FILE* m_local_file;
        FILE* m_log_file;

protected:
        void preprocess(const char* local_file_openmode) {
                //open local_file
                m_local_file = fopen(local.c_str(), local_file_openmode);

                //open log file
                m_log_file = fopen(log.c_str(), "a");
                if(m_log_file != 0) {
                        time_t t = time(0);
                        tm* cur_time = localtime(&t);
                        size_t ret = fprintf(m_log_file, "******** %s", asctime(cur_time));
                        curl_easy_setopt(m_curl, CURLOPT_VERBOSE, 1);
                        curl_easy_setopt(m_curl, CURLOPT_STDERR, m_log_file);
                } else {
                        curl_easy_setopt(m_curl, CURLOPT_VERBOSE, 0);
                }

                curl_easy_setopt(m_curl, CURLOPT_URL, remote.c_str());
                curl_easy_setopt(m_curl, CURLOPT_TIMEOUT_MS, timeout);
        }

        void release() {
                fclose(m_local_file);
                fputs("\n\n\n", m_log_file);
                fclose(m_log_file);
        }

public:
        std::string local;
        std::string remote;
        std::string log;

        int timeout;

public:
        netfile() : m_curl(0)
                  , m_local_file(0)
                  , m_log_file(0)
                  , timeout(0)
                {
                        curl_global_init(CURL_GLOBAL_ALL);
                        m_curl = curl_easy_init();
                }

        int upload() {
                preprocess("rb");

                verify_perform(curl_easy_setopt(m_curl, CURLOPT_UPLOAD, 1));
                verify_perform(curl_easy_setopt(m_curl, CURLOPT_READDATA, m_local_file));
                verify_perform(curl_easy_perform(m_curl));

                release();
                return 0;
        }
        
        int download() {
                preprocess("wb");

                verify_perform(curl_easy_setopt(m_curl, CURLOPT_UPLOAD, 0));
                verify_perform(curl_easy_setopt(m_curl, CURLOPT_WRITEDATA, m_local_file));
                verify_perform(curl_easy_perform(m_curl));

                release();
                return 0;
        }
};

#endif   /* ----- #ifndef netfile_INC  ----- */
