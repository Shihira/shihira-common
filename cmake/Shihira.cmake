######################################## static functions:

MACRO(S_SHIHIRA_USE_NETWORK)
        IF(WIN32)
                SET(PROJ_LIB "${PROJ_LIB};ws2_32;wsock32;wldap32")
        ENDIF(WIN32)
ENDMACRO(S_SHIHIRA_USE_NETWORK)

######################################## B.E. functions:

SET(PROJ_SRC "")
SET(PROJ_LIB "")
SET(PROJ_CFLAGS "")
SET(PROJ_LFLAGS "")
SET(SHIHIRA_COMMON_DIR "")

MACRO(SHIHIRA_PROJECT_START PROJ_NAME)
        SET(PROJ_SRC "")
        SET(PROJ_LIB "")
        PROJECT(${PROJ_NAME})
        AUX_SOURCE_DIRECTORY(../src PROJ_SRC)
        INCLUDE_DIRECTORIES(../include)
        INCLUDE_DIRECTORIES(../build)
        INCLUDE_DIRECTORIES(${SHIHIRA_COMMON_DIR})
        SET(EXECUTABLE_OUTPUT_PATH ..)
ENDMACRO(SHIHIRA_PROJECT_START)

MACRO(SHIHIRA_PROJECT_END PROJ_NAME)
        MESSAGE("-- Source File: ${PROJ_SRC}")
        MESSAGE("-- Libraries: ${PROJ_LIB}")
        MESSAGE("-- CFlags: ${PROJ_CFLAGS}")
        MESSAGE("-- LFlags: ${PROJ_LFLAGS}")
        ADD_EXECUTABLE(${PROJ_NAME} ${PROJ_SRC})
        TARGET_LINK_LIBRARIES(${PROJ_NAME} ${PROJ_LIB})
        SET_TARGET_PROPERTIES(${PROJ_NAME} PROPERTIES COMPILE_FLAGS "${PROJ_CFLAGS}")
        SET_TARGET_PROPERTIES(${PROJ_NAME} PROPERTIES LINK_FLAGS "${PROJ_LFLAGS}")
ENDMACRO(SHIHIRA_PROJECT_END)

######################################## Libraries functions:

MACRO(SHIHIRA_USE_QT)
        FIND_PACKAGE(Qt4 REQUIRED ${ARGN})
 	INCLUDE(${QT_USE_FILE})
	ADD_DEFINITIONS(${QT_DEFINITIONS})
        SET(PROJ_LIB "${PROJ_LIB};${QT_LIBRARIES}")
        INCLUDE_DIRECTORIES(.)
ENDMACRO(SHIHIRA_USE_QT)

MACRO(SHIHIRA_USE_TINYXML)
        FIND_PACKAGE(TinyXML)
        SET(PROJ_LIB "${PROJ_LIB};${TINYXML_LIBRARIES}")
        INCLUDE_DIRECTORIES(${TINYXML_INCLUDE_DIR})
ENDMACRO(SHIHIRA_USE_TINYXML)

MACRO(SHIHIRA_USE_CURL)
        FIND_PACKAGE(CURL)
        SET(PROJ_LIB "${PROJ_LIB};${CURL_LIBRARIES}")
        INCLUDE_DIRECTORIES(${CURL_INCLUDE_DIR})
        S_SHIHIRA_USE_NETWORK()
ENDMACRO(SHIHIRA_USE_CURL)

MACRO(SHIHIRA_USE_BOOST)
        FIND_PACKAGE(Boost REQUIRED ${ARGN})
        SET(PROJ_LIB "${PROJ_LIB};${Boost_LIBRARIES}")
        INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIR})
ENDMACRO(SHIHIRA_USE_BOOST)

######################################## executables:

MACRO(SHIHIRA_ADD_QT_MOC H_TO_MOC MOC_OUT)
        IF(QT4_FOUND)
                ADD_CUSTOM_COMMAND(
                        OUTPUT ${MOC_OUT}
                        COMMAND ${QT_MOC_EXECUTABLE} ../include/${H_TO_MOC} -o ${MOC_OUT}
                        DEPENDS ../include/${H_TO_MOC}
                )
                SET(PROJ_SRC "${PROJ_SRC};${MOC_OUT}")
        ENDIF(QT4_FOUND)
ENDMACRO(SHIHIRA_ADD_QT_MOC)

MACRO(SHIHIRA_ADD_QT_UIC UI_TO_UIC UIC_OUT)
        IF(QT4_FOUND)
                ADD_CUSTOM_COMMAND(
                        OUTPUT ${UIC_OUT}
                        COMMAND ${QT_UIC_EXECUTABLE} ../include/ui/${UI_TO_UIC} -o ${UIC_OUT}
                        DEPENDS ../include/ui/${UI_TO_UIC}
                )
                SET(PROJ_SRC "${PROJ_SRC};${UIC_OUT}")
        ENDIF(QT4_FOUND)
ENDMACRO(SHIHIRA_ADD_QT_UIC)

MACRO(SHIHIRA_USE_RES)
        IF(WIN32)
                ADD_CUSTOM_COMMAND(
                        OUTPUT resource.res
                        COMMAND windres ../images/resource.rc -O coff -o resource.res
                        DEPENDS ../images/resource.rc
                )
                SET(PROJ_SRC "${PROJ_SRC};resource.res")
                SHIHIRA_LFLAGS("resource.res")
        ENDIF(WIN32)
ENDMACRO(SHIHIRA_USE_RES)

MACRO(SHIHIRA_ADD_BIN BIN_TO_H H_OUT)
        ADD_CUSTOM_COMMAND(
                OUTPUT ${H_OUT}
                COMMAND python ${SHIHIRA_COMMON_DIR}/b2h.py -f ../images/${BIN_TO_H} -o ${H_OUT}
                DEPENDS ../images/${BIN_TO_H}
        )
        SET(PROJ_SRC "${PROJ_SRC};${H_OUT}")
ENDMACRO(SHIHIRA_ADD_BIN)

######################################## flags:

MACRO(SHIHIRA_CFLAGS VALUE)
        SET(PROJ_CFLAGS "${PROJ_CFLAGS} ${VALUE}")
ENDMACRO(SHIHIRA_CFLAGS)

MACRO(SHIHIRA_LFLAGS VALUE)
        SET(PROJ_LFLAGS "${PROJ_LFLAGS} ${VALUE}")
ENDMACRO(SHIHIRA_LFLAGS)

